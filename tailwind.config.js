/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
	theme: {
		colors: {
			red: {
				gfinity: '#E94235'
			},
			blue: {
				electric: '#293894'
			},
			gray: {
				dark: '#1B1B1B',
				light: '#6B6B65'
			},
			concrete: '#CFD1C7',
			sand: '#DFE0D9',
			white: '#FFFFFF',
			background: {
				from: '#E8E9E3',
				to: '#CFD1C7'
			},
			transparent: 'transparent'
		},
		fontFamily: {
			sans: ['Akzidenz-Grotesk', 'sans-serif']
		},
		fontSize: {
			'title-super': [
				'48px',
				{ letterSpacing: '-3px', lineHeight: '44px' }
			],
			'title-large': [
				'36px',
				{ letterSpacing: '-2px', lineHeight: '44px' }
			],
			body: ['18px', { letterSpacing: '-0.5px', lineHeight: '22px' }],
			'callout-medium': [
				'16px',
				{ letterSpacing: '-0.5px', lineHeight: '19px' }
			],
			'callout-small': [
				'14px',
				{ letterSpacing: '-0.5px', lineHeight: '17px' }
			],
			footnote: ['12px', { letterSpacing: '0px', lineHeight: '14px' }],
			'caption-small': [
				'12px',
				{ letterSpacing: '1.5px', lineHeight: '12px' }
			],
			badge: ['10px', { letterSpacing: '1.5px', lineHeight: '12px' }]
		},
		extend: {
			backgroundImage: {
				'gradient-radial':
					'radial-gradient(var(--gradient-color-stops))'
			},
			width: {
				'14': '56px',
				'22': '86px',
				'26': '105px'
			},
			height: {
				'14': '56px',
				'22': '86px',
				'26': '105px'
			},
			spacing: {
				'22': '86px',
				'26': '105px'
			},
			scale: {
				'99': '.99'
			},
			zIndex: {
				'-1': '-1',
				'-10': '-10'
			},
			opacity: {
				'80': '80'
			}
		}
	},
	variants: {},
	plugins: []
}
