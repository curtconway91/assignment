import { cdn } from '~/plugins/helpers'

export class User {
	constructor({ id, name, avatar, rank }) {
		this.id = id
		this.name = name
		this.avatar = cdn(avatar)
		this.rank = rank
	}
}

export class TeamMember {
	constructor({ user, readyState }) {
		this.user = user
		this.readyState = readyState
	}
}

export const CurrentUser = new User({
	id: 1,
	name: 'steveroesler',
	avatar: 'avatars/gf-avatar-01_mklwfg.png',
	rank: 74
})

export const Friends = [
	new User({
		id: 2,
		name: 'AkuAku',
		avatar: 'avatars/gf-avatar-02_ptvrm4.png',
		rank: 53
	}),
	new User({
		id: 3,
		name: 'Richard',
		avatar: 'avatars/gf-avatar-03_nizknb.png',
		rank: 23
	}),
	new User({
		id: 4,
		name: 'machinaman',
		avatar: 'avatars/gf-avatar-04_gjhhy4.png',
		rank: 67
	}),
	new User({
		id: 5,
		name: 'CaptainAmeowica',
		avatar: 'avatars/gf-avatar-05_br2dm7.png',
		rank: 98
	}),
	new User({
		id: 6,
		name: 'Rafiki',
		avatar: 'avatars/gf-avatar-06_ivd0ou.png',
		rank: 35
	})
]
