export const GAME_TYPES = {
	Competitive: 'Competitive',
	Casual: 'Casual'
}

export const GAME_MODES = {
	SquadBattles: 'Squad Battles',
	UltimateTeam: 'Ultimate Team',
	KickOff: 'Kick Off',
	SkillGames: 'Skill Games',
	PracticeArena: 'Practice Arena'
}

export const LOBBY_STATUSES = {
	InviteOnly: 'Invite Only',
	Public: 'Public',
	Password: 'Password'
}
