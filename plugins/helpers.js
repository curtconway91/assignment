import Vue from 'vue'

/**
 * @param {string} path
 * @returns {string}
 */
export function cdn(path = '') {
	return `${process.env.cdnURL}/${path}`
}

/**
 * @param {string} name
 * @returns {string}
 */
export function possessivize(name) {
	let output = `${name}’`
	if (name[name.length - 1].toLowerCase() !== 's') {
		output += 's'
	}
	return output
}

Vue.use({
	install() {
		Vue.cdn = cdn
		Vue.prototype.$cdn = cdn
	}
})
