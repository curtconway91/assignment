import Vue from 'vue'
import Components from '~/components'

Vue.use({
	install(Vue, options) {
		Object.keys(Components).forEach(key => {
			Vue.component(Components[key].name, Components[key])
		})
	}
})
