import { CurrentUser, Friends } from '~/assets/js/users'

export const state = () => ({
	currentUser: CurrentUser,
	friends: Friends
})
