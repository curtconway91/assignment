import { GAME_TYPES, GAME_MODES, LOBBY_STATUSES } from '~/assets/js/constants'
import { TeamMember } from '~/assets/js/users'

export const state = rootState => ({
	gameType: GAME_TYPES.Competitive,
	gameMode: GAME_MODES.SquadBattles,
	lobbyStatus: LOBBY_STATUSES.InviteOnly,
	usersInQueue: 3021,
	leader: null,
	team: []
})

export const mutations = {
	setGameType(state, gameType) {
		state.gameType = gameType
	},

	setGameMode(state, gameMode) {
		state.gameMode = gameMode
	},

	setLobbyStatus(state, lobbyStatus) {
		state.lobbyStatus = lobbyStatus
	},

	setLeader(state, user) {
		state.leader = new TeamMember({ user, readyState: true })
	},

	addTeamMember(state, user) {
		state.team.push(new TeamMember({ user, readyState: true }))
	},

	removeTeamMember(state, user) {
		const index = state.team.indexOf(user)
		if (index > -1) {
			state.team.splice(index, 1)
		}
	}
}

export const actions = {
	setLeader(context, user) {
		context.commit('setLeader', user)
	},

	addTeamMember(context, user) {
		context.commit('addTeamMember', user)
	},

	removeTeamMember(context, user) {
		context.commit('removeTeamMember', user)
	},

	setLobbyOption(context, { row, option }) {
		switch (row) {
			case 'Game Types':
				context.commit('setGameType', option)
				break
			case 'Lobby Status':
				context.commit('setLobbyStatus', option)
				break
			case 'Gamemode':
				context.commit('setGameMode', option)
				break
			default:
				break
		}
	}
}
