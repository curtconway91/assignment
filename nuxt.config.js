require('dotenv').config()

module.exports = {
	env: {
		cdnURL: process.env.CDN_URL
	},
	mode: 'universal',
	/*
	 ** Headers of the page
	 */
	head: {
		title: process.env.npm_package_name || '',
		meta: [
			{ charset: 'utf-8' },
			{
				name: 'viewport',
				content: 'width=device-width, initial-scale=1'
			},
			{
				hid: 'description',
				name: 'description',
				content: process.env.npm_package_description || ''
			}
		],
		link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
	},
	/*
	 ** Customize the progress-bar color
	 */
	loading: { color: '#fff' },
	/*
	 ** Global CSS
	 */
	css: ['~assets/scss/style.scss'],
	/*
	 ** Plugins to load before mounting the App
	 */
	plugins: ['~/plugins/helpers.js', '~/plugins/components.js'],
	/*
	 ** Nuxt.js dev-modules
	 */
	buildModules: [
		// Doc: https://github.com/nuxt-community/svg-module
		'@nuxtjs/svg',
		// Doc: https://github.com/nuxt-community/eslint-module
		'@nuxtjs/eslint-module',
		// Doc: https://github.com/nuxt-community/nuxt-tailwindcss
		'@nuxtjs/tailwindcss'
	],
	/*
	 ** Nuxt.js modules
	 */
	modules: [
		// Doc: https://axios.nuxtjs.org/usage
		'@nuxtjs/axios',
		'@nuxtjs/dotenv'
	],
	/*
	 ** Axios module configuration
	 ** See https://axios.nuxtjs.org/options
	 */
	axios: {},
	/*
	 ** Build configuration
	 */
	build: {
		devtools: true,
		/*
		 ** You can extend webpack config here
		 */
		extend(config, ctx) {}
	},
	server: {
		port: process.env.PORT || 3000,
		host: '0.0.0.0'
	},
	vue: {
		config: {
			productionTip: false,
			devtools: true
		}
	}
}
